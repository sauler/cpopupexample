/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CPopupExample for AQQ                                  *
 *                                                                             *
 * CPopupExample plugin is free software: you can redistribute it and/or       *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CPopupExample is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include <vcl.h>
#include <windows.h>

// SDK includes
#include "SDK\PluginLink.h"
#include "SDK\Popup.h"
#include "SDK\PopupMenuItem.h"
#include "SDK\StateButton.h"
#include "SDK\PluginAPI.h"

// Plugin includes
#include "ServiceHandler.h"


TPluginInfo PluginInfo;

CPopupMenuItem* PopupMenuItem;
CPopup* Popup;
CStateButton* Button;

CServiceHandler* ServiceHandler;

int WINAPI DllEntryPoint(HINSTANCE hinst,unsigned long reason,void* lpReserved)
{
	return 1;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Load(PPluginLink Link)
{
	CPluginLink::instance()->SetLink(*Link);

	ServiceHandler = new CServiceHandler();


	PopupMenuItem = new CPopupMenuItem("MyItem", "Example", "N4", -1, "MyItemService", "muItem");
	PopupMenuItem->Create();


   	Popup = new CPopup("MyPopup");
	Popup->AddItem(new CPopupMenuItem("MyPopupItem", "Example", 0, 34, "MyPopupItemService"));
	Popup->AddItem(new CPopupMenuItem("MyPopupItem1", "Example1", 1, 35, "MyPopupItemService1"));

	Button = new CStateButton();
	Button->BlinkStartIcon = 35;
	Button->BlinkStopIcon = 34;
	Button->CreateButton("MyButton", 0, 35, "", "MyPopup");
	Button->Blink(5);

	return 0;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Unload()
{
    delete PopupMenuItem;
	delete Popup;
	delete Button;
	delete ServiceHandler;

	return 0;
}

extern "C" __declspec(dllexport) PPluginInfo __stdcall AQQPluginInfo
	(DWORD AQQVersion)
{
	PluginInfo.cbSize = sizeof(TPluginInfo);
	PluginInfo.ShortName = L"CPopupExample";
	PluginInfo.Version = PLUGIN_MAKE_VERSION(1,0,0,0);
	PluginInfo.Description =
		L"Przyk�adowa wtyczka demonstruj�ca tworzenie przycisku z popupem i element�w w popupie za pomoc� SDK.";
	PluginInfo.Author = L"Rafa� Babiarz (sauler)";
	PluginInfo.AuthorMail = L"sauler1995@gmail.com";
	PluginInfo.Copyright = L"sauler";
	PluginInfo.Homepage = L"";
	PluginInfo.Flag = 0;
	PluginInfo.ReplaceDefaultModule = 0;

	return &PluginInfo;
}

