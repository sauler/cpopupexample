/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CPopupExample for AQQ                                  *
 *                                                                             *
 * CPopupExample plugin is free software: you can redistribute it and/or       *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CPopupExample is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "ServiceHandler.h"
#include "SDK\PluginLink.h"
#include "SDK\AQQ.h"
#include "CPopupExample.h"


CServiceHandler::CServiceHandler()
{
	CPluginLink::instance()->GetLink().CreateServiceFunction(L"MyItemService", ItemService);
	CPluginLink::instance()->GetLink().CreateServiceFunction(L"MyPopupItemService", PopupItemService);
	CPluginLink::instance()->GetLink().CreateServiceFunction(L"MyPopupItemService1", PopupItemService1);
}

CServiceHandler::~CServiceHandler()
{
	CPluginLink::instance()->GetLink().DestroyServiceFunction(ItemService);
	CPluginLink::instance()->GetLink().DestroyServiceFunction(PopupItemService);
	CPluginLink::instance()->GetLink().DestroyServiceFunction(PopupItemService1);
}

INT_PTR __stdcall CServiceHandler::ItemService(WPARAM wParam, LPARAM lParam)
{
	AQQ::Functions::ShowMessage(0, "MyItemService");
}

INT_PTR __stdcall CServiceHandler::PopupItemService(WPARAM wParam,LPARAM lParam)
{
	Button->IconID = 34;
	Button->Update();
	return 0;
}

INT_PTR __stdcall CServiceHandler::PopupItemService1(WPARAM wParam,LPARAM lParam)
{
	Button->IconID = 35;
	Button->Update();
	return 0;
}
