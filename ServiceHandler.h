/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of CPopupExample for AQQ                                  *
 *                                                                             *
 * CPopupExample plugin is free software: you can redistribute it and/or       *
 * modify it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * CPopupExample is distributed in the hope that it will be useful,            *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include <windows.h>

class CServiceHandler
{
public:
	CServiceHandler();
	~CServiceHandler();
	static INT_PTR __stdcall ItemService(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall PopupItemService(WPARAM wParam, LPARAM lParam);
	static INT_PTR __stdcall PopupItemService1(WPARAM wParam, LPARAM lParam);
};
